
#! /bin/bash

usage(){
	echo "Usage: shapeToPSql.sh sridfrom sridto shp tblName dbname passwd"
	echo 'Ex: shapeToSql 4326 vqd_quartiers.shp quartiers qcneigh passwd'
	echo 'Ex (cont): shp2pgsql -s 4326 vqd_quartiers.shp quartiers | PGPASSWORD=passwd psql --host=localhost --user=$USER --dbname=qcneigh' 
}



if [ $# == 6 ] ; then
	shp2pgsql -s $1:$2 $3 $4 | PGPASSWORD=$6 psql --host=localhost --user=$USER --dbname=$5
else
	echo 'Error!'
	usage

fi
