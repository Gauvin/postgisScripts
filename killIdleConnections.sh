
#! /bin/bash


function usage(){
	echo "Usage: killIleConnection tblName [-f FORCE]"
}


function get_connections_to_kill(){

    pid_to_kill=$(ps -ef | grep postgres | grep $1 | awk '/[0-9]/{print $2}' )
    pid_to_kill_count=$(ps -ef | grep postgres | grep -c $1)

    if [ $2 == "True" ]; then 
        echo -e "There are $pid_to_kill_count processes to kill:\n$pid_to_kill"
    fi
}


function kill_connection(){

    get_connections_to_kill $1 "False"
    echo "Will terminate the following PID: $pid_to_kill "
    sudo kill $pid_to_kill
}


args=("$@")

if [ $# == 1 ]; then
    get_connections_to_kill $1 "True"
elif [ $# == 2 ] && [ "${args[1]}" == '-f' ]; then
    get_connections_to_kill $1 "False"
    kill_connection $1
else
    usage 
fi


