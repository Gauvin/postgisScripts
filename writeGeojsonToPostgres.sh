
#! /bin/bash

usage(){
	echo "Usage: writeGeojsonToPostgres.sh dbname tblName jsonFile passwd sridfrom sridto"
}

#Get the path of programs
geoPath=`which geojsplit`
pythonPath=`which python`

main(){

	if [ $# -eq 6 ] ; then
		
		#First make sure the geojson file exists
		if [ ! -f "${3}" ]; then
		    	echo "Fatal error! file ${3} does not exist!"
			return 1
		fi


		#Check if geojsplit has been installed or not
		
		if [ ${#geoPath} -eq 0 ] ; then
			echo "make sure geojsplit is installed for ${pythonPath} by running sudo pip3 install geojsplit"
			return 1
		fi




		#Create a tmp dir + copy the large geojson there
		rm -rf tmp
		mkdir tmp
		cd tmp
		cp ../"${3}" ./"${3}"

		#Split the geojson 
		geojsplit "${3}" 
		if [ $? -ne 0 ] ; then
			geoResults="ERROR"
		else
			geoResults="OK"
		fi
		echo "Results: geojson results: ${geoResults}"


		#Need to remove the large copied geojson (do this in any case)
		rm -f ./"${3}"


		#Check results of geojsplit and exit if unsucccessfull
		if [ $geoResults == "ERROR" ]; then
		    	echo 'geojsplit failed! aborting'
			return 1
		fi



		#For each new smaller geojson, output to the same table in postgis db and append 
		#Need this counter bs because we dont know what columns need to be created and shp2pgsql will do it automatically
		#Running PGPASSWORD=$4 psql --host=localhost --user=$USER --dbname=$1 -c "CREATE TABLE $2 ();" is no good
		counter=0
		for f in ./*.geojson; do

			echo "${f}"

			#This intermediary conversion to shp step should not be required
	 		ogr2ogr -f "ESRI Shapefile" genShp.shp "${f}" 

			#Create a new table
			if [ $counter -eq 0 ]; then
				shp2pgsql -s $5:$6 genShp.shp $2  | PGPASSWORD=$4 psql --host=localhost --user=$USER --dbname=$1 
				counter=$((counter+1))
			#Append
			else
				shp2pgsql -a -s $5 genShp.shp $2  | PGPASSWORD=$4 psql --host=localhost --user=$USER --dbname=$1 
			fi
				

		done  

		#Remove the tmp directory
		cd ..
		rm -rf tmp

	else
		echo "Error! ${#} arguments passed - 6 required"
		usage

	fi

}

#Using quotes to deal with names with spaces
main "${1}" "${2}" "${3}" "${4}" "${5}" "${6}"
