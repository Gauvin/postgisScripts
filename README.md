# Simple postgis scripts

This is a collection of (shitty) bash, r and python scripts to write geojsons, shp files, etc into postgis databases.

*Note*: Most scripts require running createPostGISDB.sh and creating the db firshand before we can add any new tables or features.


## writeGeojsonToPostgres example usage 

writeGeojsonToPostgres.sh ms_buildings qc_buildings Quebec.geojson $pass 102002

where the env variable pass can for instance be set through
export pass=****

*Note*: Need to have geojsplit python package installed (ideally for root also through sudo pip3 install geojsplit so that it can be called from other python programs)
https://medium.com/@underchemist/splitting-geojson-files-using-geojsplit-8ff72ec68c67

*Note*: !! The script needs to be run from the directory where Quebec.geojson is located !!

## shapeToSql.sh example usage

shapeToSql 4326 vqd_quartiers.shp quartiers qcneigh

*Note*: this is pretty much a useless script that doesn't simplify the process much


## writeShpToPostgres.R 

Rscript pathToScript/writeShpToPostgres.R -d dbName -s shpName

*Note*: Advantage of using sf and R is that the column types + projection are automatically identified. 

In addition, the script takes care of nasty utf characters by replacing them by "". This can be helpful


## getProjectionEPSG.R 

Rscript pathToScript/getProjectionEPSG.R -s shpName


Parses the .prj file an returns the EPSG id as a single character.


## Note to self on how to make scripts available to all (not necessarily useful)

Take a .sh in a /home/user/ and make it available to all users - e.g. also to postgres

* [location of scripts as per ubuntu convention]
    - /usr/local/

* [command for symbolic link]
    - sudo ln -s ~/scripts/ /usr/local/
    - sudo ln -s ~/postgisScripts/ /usr/local/

* [additional info]
    - /etc/profile/
    - export PATH=$PATH:/usr/local/scripts
    - export PATH=$PATH:/usr/local/postgisScripts


