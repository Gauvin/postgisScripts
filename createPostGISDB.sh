#! /bin/bash


usage(){
	echo "Usage: createPostGISDB.sh dbName [optional] user "
	echo 'Log in with sudo su - postgres first'
	echo 'Currently grants access to charles'
}



if [ $# == 1 ]; then

	if [ $USER = 'postgres' ]; then
		#Create the db with name $1 
		createdb --username=postgres --owner=postgres $1
		#add in postgis support -- this is important
		psql -d $1 -c "CREATE EXTENSION postgis;" 
		#grand access to users (hard coded in this case)
		#this requires running createuser --interactive --pwprompt first
		psql -d $1 -c "GRANT ALL PRIVILEGES ON DATABASE $1 TO charles;"
		echo "Successfully created table $1 and granted all accesses to charles"
		exit
	else
		usage
	fi
elif [ $# == 2 ]; then

	createdb --username=$2 --owner=$2 $1
			psql -d $1 -U $2 -c "CREATE EXTENSION postgis;" #add in postgis support -- this is important - otherwise the geometry is buggy and useless
			echo "Successfully created table $1 for user $2"
			exit

else 
	echo "Error! need DB name"
	usage
fi
