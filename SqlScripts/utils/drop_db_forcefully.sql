--Disconnect from the DB first
--might want to run a ps -ef | grep postgres first and kill idle connections with sudo kill PID
SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'osm_db'
  AND pid <> pg_backend_pid();

--Now drop the db
DROP DATABASE osm_db;
