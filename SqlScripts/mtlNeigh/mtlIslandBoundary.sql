CREATE TABLE if not exists mtl_entire_island_boundary as 
(
	SELECT st_union(geometry)
	FROM public.da_mtl_entire_island
) 