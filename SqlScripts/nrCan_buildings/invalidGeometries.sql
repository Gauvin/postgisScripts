
-- Spatial join with invalid geometries
SELECT * 
FROM (
	SELECT  q_socio,geom
	FROM public.quartiers_sociologiques_2014 
	WHERE q_socio LIKE 'Villeray' ) as neigh
JOIN 
	(SELECT * 
	FROM mtl_buildings 
	WHERE  St_isvalid (geom)
	 )as buildings
ON st_intersects(neigh.geom, buildings.geom)

