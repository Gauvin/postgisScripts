--Sets the srid to 2958 - utm 17 + then reprojects to 4326 
ALTER TABLE toronto_segments
ALTER COLUMN geom TYPE geometry(MULTILINESTRING, 4326) USING ST_Transform(ST_SetSRID(geom,2958),4326 ) ;
 
