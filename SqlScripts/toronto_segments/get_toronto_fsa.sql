
create table if not exists fsa_cma_toronto as
select cfsauid,  fsa.geom as fsa_geom , segments.*
from  public.fsa_c_canada as fsa
join  public.toronto_segments as segments
on st_intersects(fsa.geom, segments.geom);


ALTER TABLE fsa_cma_toronto
DROP COLUMN geom;

ALTER TABLE fsa_cma_toronto
RENAME column fsa_geom to geometry;