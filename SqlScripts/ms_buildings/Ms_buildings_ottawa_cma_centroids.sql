create table cma_ottawa_allcma_buildings_unique_centroids as
select st_centroid(geom) as geom,"gid",
"fid",
"Population",
"Households",
"GeoUID",
"Type",
"CD_UID",
"Shape Area",
"Dwellings",
"CSD_UID",
"CT_UID",
"CMA_UID"
from public.cma_ottawa_allcma_buildings_unique