  	
create table if not exists 	cma_ottawa_allcma_buildings_unique as 
select * 
from cma_ottawa_allcma_buildings_unique;
	
DELETE FROM cma_ottawa_allcma_buildings_unique as a
USING
	(SELECT MIN(ctid) as ctid, gid
        FROM public.cma_ottawa_allcma_buildings_unique
        GROUP BY gid HAVING COUNT(*) > 1
      ) as b
WHERE a.gid = b.gid 
AND a.ctid <> b.ctid;