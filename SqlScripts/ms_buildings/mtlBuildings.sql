
-- For reference only (explicitely setting srid is required for spatial join)
-- Will not work anymore
-- Based on neighbourhoods rather than the entire island polygon or some census dataset
CREATE TABLE if not exists mtl_buildings as
(
SELECT  neigh.nom_qr, neigh.nom_arr, ms_buildings.geom
FROM (
	SELECT ST_SetSRID(geom,4326) as geom 
	FROM public.qc_buildings_cleaned
	WHERE st_isvalid(geom)
	) as ms_buildings
JOIN (
	SELECT nom_qr, nom_arr,  ST_Transform(geometry,4326) as geom
	FROM public."neighHousing"
	) as neigh
ON st_within(ms_buildings.geom, neigh.geom)
)
