-- Inserted a table with 113188 rows twice
-- Neede to remove those last records
-- Sort by ctid and remove those
DELETE FROM ottawa_qc_buildings
WHERE ctid IN (
    SELECT ctid
    FROM ottawa_qc_buildings  
	ORDER BY ctid DESC
    LIMIT 113188
)
