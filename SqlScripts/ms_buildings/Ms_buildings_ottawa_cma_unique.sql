-- Ottawa CMA - ontario side
create table if not exists cma_ottawa_on_buildings_unique as
select * 
from public.pr_on_buildings as buildings
join 
(
	select st_transform( st_union(st_buffer(st_transform(geometry,3347),10)) , 4326) as geometry
	from public.da_cma_ottawa
)as censusUnion
on st_intersects( buildings.geom,censusUnion.geometry);





-- Ottawa CMA - quebec side
SELECT UpdateGeometrySRID('pr_qc_buildings','geom',4326);

create table if not exists cma_ottawa_qc_buildings_unique as
select * 
from public.pr_qc_buildings as buildings
join 
(
	select st_transform( st_union(st_buffer(st_transform(geometry,3347),10)) , 4326) as geometry
	from public.da_cma_ottawa
)as censusUnion
on st_intersects(buildings.geom,  census.geometry);

alter table cma_ottawa_qc_buildings_unique
drop column if exists geometry;

-- Merge
create table if not exists cma_ottawa_allcma_buildings_unique as 
select *
from cma_ottawa_on_buildings_unique;

insert into cma_ottawa_allcma_buildings_unique 
select *
from cma_ottawa_qc_buildings_unique
WHERE NOT EXISTS(
        select * 
		FROM cma_ottawa_allcma_buildings_unique
  );
  
  
  