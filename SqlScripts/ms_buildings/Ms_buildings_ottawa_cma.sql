-- Ottawa CMA - ontario side
create table if not exists cma_ottawa_on_buildings as
select * 
from public.pr_on_buildings as buildings
join public.da_cma_ottawa as census
on st_intersects(census.geometry, buildings.geom)

-- Ottawa CMA - quebec side
SELECT UpdateGeometrySRID('pr_qc_buildings','geom',4326)

create table if not exists cma_ottawa_qc_buildings as
select * 
from public.pr_qc_buildings as buildings
join public.da_cma_ottawa as census
on st_intersects(census.geometry, buildings.geom);

alter table cma_ottawa_qc_buildings 
drop column if exists geometry;

-- Merge
create table if not exists cma_ottawa_allcma_buildings as 
select *
from cma_ottawa_on_buildings

insert into cma_ottawa_allcma_buildings 
select *
from cma_ottawa_qc_buildings
WHERE NOT EXISTS(
        select * 
		FROM cma_ottawa_allcma_buildings
  )
  
  
  