CREATE TABLE mtl_buildings if not exist as
(
SELECT * 
FROM (
	SELECT ST_SetSRID(geom,4326) as geom 
	FROM public.qc_buildings_cleaned
	) as ms_buildings
JOIN (
	SELECT q_socio, arrondisse,  ST_Transform(geom,4326) as geom
	FROM public.quartiers_sociologiques_2014 
	) as neigh
ON st_within(ms_buildings.geom, neigh.geom)
)
