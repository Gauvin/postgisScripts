
create table if not exists cma_ottawa_allcma_intersection_build_weights as 
select count(distinct( id )) as weights, mergedTbl.fid as fid
from 
	(
	select * 
	from public.cma_ottawa_allcma_buildings_unique as buildings
	join public.ottawa_allcma_96_16_intersection as inter
	on st_intersects(buildings.geom, inter.geometry )
	) as mergedTbl
group by mergedTbl.fid

alter table cma_ottawa_allcma_buildings_stJoin_inter
drop column 