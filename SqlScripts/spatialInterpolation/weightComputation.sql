alter table public.ottawa_allcma_96_16_intersection add column "weights" double precision;

update public.ottawa_allcma_96_16_intersection as initTbl
set weights = groupedTbl.weights
from 
(
select count(distinct(gid)) as weights , shpInter.id as id
from public.ottawa_allcma_96_16_intersection  as shpInter
join  public.cma_ottawa_allcma_buildings_unique  as buildings 
on st_intersects(shpInter.geometry, buildings.geom )
group by shpInter.id
) as groupedTbl
where initTbl.id = groupedTbl.id 