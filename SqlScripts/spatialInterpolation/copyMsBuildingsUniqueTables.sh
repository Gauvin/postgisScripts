#! /bin/bash

#Copy the unique buildings
pg_dump -t cma_ottawa_allcma_buildings_unique ms_buildings | psql spatialInterpolation

#Copy the unique buildings centroids
pg_dump -t cma_ottawa_allcma_buildings_unique_centroids ms_buildings | psql spatialInterpolation

