
#! /bin/bash


function usage(){
	echo "Usage: osm1pgsql.sh DBNAME PBFFILE PORTNUMBER"
 
}


function check_params(){

    if [ "$#" -ne 3 ]; then 
        echo "Incorrect numer of parameters!"
        usage
        return 1
    fi

}


function createdb_custom(){
    echo "$1 $2"
    createdb -U charles -W -p $2 $1
    psql -d $1 -c 'CREATE EXTENSION postgis; CREATE EXTENSION hstore;'
}

function main(){

    osm2pgsql_path=$(which osm2pgsql)
    if [ ${#osm2pgsql_path} -eq 0 ]; then
        echo 'Fatal error, must install osm2pgsql_path first!'
        return 1
    fi

    #Check table existence and create if required
    if psql -lqt | cut -d \| -f 1 | grep $1; then
        echo "table $1, already exists..."
    else
        echo "creating table $1, which does not exist..."
        createdb_custom $1 $3
    fi

    osm2pgsql --slim --username charles -W --database $1 $2 -P $3
    if [ $? -ne 0 ] ; then
         echo "Fatal error!"
    fi



}



check_params $1 $2 $3
if [ $? -eq 0 ] ; then
    main $1 $2 $3
fi